/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.jdbc.info;

import cn.jfast.framework.base.util.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Table {
    /** 实际表名 */
    private String dbName;
    /** 表名对应对象名小写 */
    private String nickName;
    /** 表类型 */
    private String type;
    /** 表所在编目 */
    private String catalog;
    /** 表所在模式 */
    private String schema;
    /** 说明 */
    private String remarks;
    /** 表列 */
    private Map<String,Column> column = new HashMap<String,Column>();

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /** 表主键 */
    private Set<String> primaryKey = new HashSet<String>();
    /** 自增键 */
    private Set<String> incrementKey = new HashSet<String>();

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String name) {
        this.dbName = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Map<String, Column> getColumn() {
        return column;
    }

    public void setColumn(Map<String, Column> column) {
        this.column = column;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void addColumn(Column column){
        this.column.put(StringUtils.dbColumn2ModelColumn(column.getName()),column);
    }

    public void addPrimaryKey(String column){
        this.primaryKey.add(column);
    }

    public Column getColumn(String columnName){
        return this.column.get(columnName);
    }

    public String[] getPrimaryKey(){
        return this.primaryKey.toArray(new String[]{});
    }
    
    public String[] getAutoIncrementKey(){
    	for(String primary:this.getPrimaryKey()){
    		if(getColumn(primary).getAutoIncrement().toLowerCase().equals("yes")){
    			incrementKey.add(primary);
    		}
    	}
    	if(incrementKey.size() > 1)
    		throw new RuntimeException("jfast 只支持单个自增字段 ");
    	return incrementKey.toArray(new String[]{});
    }

}
