package cn.jfast.plugin.util;

public class StringUtils {
	
	public static String dbColumn2ModelColumn(String dbColumn) {
		return dbColumn.replaceAll("\\W|_", "").toLowerCase();
	}
	
	public static boolean isEmpty(String str){
		return null == str||str.equals("");
	}
	
	public static String firstCharToUpperCase(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'a' && firstChar <= 'z') {
			char[] arr = str.toCharArray();
			arr[0] -= ('a' - 'A');
			return new String(arr);
		}
		return str;
	}
}
