/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.plugin.db;

import java.sql.Types;

import cn.jfast.plugin.util.StringUtils;

public class Column {
	/** 字段名 */
	private String name;
	/** 字段类型 */
	private int type;
	/** 允许为空 */
	private String nullAble;
	/** 字段长度 */
	private Integer size;
	/** 默认值 */
	private String defaultValue;
	/** 是否自增 */
	private String autoIncrement;
	/** 说明 */
	private String remarks;
	/** 数据库字段对应java字段类型 */
	private String javaType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String[] nameField = name.split("_");
		String finalName = nameField[0];
		for(int i = 1;i<nameField.length;i++){
			finalName+= StringUtils.firstCharToUpperCase(nameField[i].toLowerCase());
		}
		this.name = finalName;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
		if(type == Types.DECIMAL){
			setJavaType("BigDecimal");
		} else if(type == Types.DATE 
				|| type == Types.TIME 
				|| type == Types.TIMESTAMP){
			setJavaType("Date");
		} else if(type == Types.BOOLEAN 
				|| (type == Types.TINYINT 
				&& null != size 
				&& 1 == size)){
			setJavaType("Boolean");
		} else if(type == Types.DOUBLE){
			setJavaType("Double");
		} else if(type == Types.FLOAT){
			setJavaType("Float");
		} else if(type == Types.SMALLINT 
				|| type == Types.INTEGER 
				|| type == Types.TINYINT
				|| type == Types.BIGINT){
			setJavaType("Integer");
		} else {
			setJavaType("String");
		} 
	}

	public String getNullAble() {
		return nullAble;
	}

	public void setNullAble(String nullAble) {
		this.nullAble = nullAble;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getAutoIncrement() {
		return autoIncrement;
	}

	public void setAutoIncrement(String autoIncrement) {
		this.autoIncrement = autoIncrement;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
}
