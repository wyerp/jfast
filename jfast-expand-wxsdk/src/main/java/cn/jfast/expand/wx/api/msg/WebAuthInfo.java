package cn.jfast.expand.wx.api.msg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 微信开放平台网页授权公众号信息
 */
public class WebAuthInfo {
	/**
	 * Token令牌
	 */
	@JSONField(name = "access_token")
	private String accessToken;
	/**
	 * 有效期
	 */
	@JSONField(name = "expires_in")
	private String expiresIn;
	/**
	 * 用户刷新access_token
	 */
	@JSONField(name = "refresh_token")
	private String refreshToken;
	/**
	 * 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID
	 */
	@JSONField(name = "openid ")
	private String openid;
	/**
	 * 用户授权的作用域，使用逗号（,）分隔
	 */
	@JSONField(name = "scope")
	private String scope;
	/**
	 * 用户授权的作用域，使用逗号（,）分隔
	 */
	@JSONField(name = "unionid")
	private String unionId;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getOpenId() {
		return openid;
	}

	public void setOpenId(String openid) {
		this.openid = openid;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUnionId() {
		return unionId;
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}

}
