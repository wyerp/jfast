package cn.jfast.expand.wx.api.kit;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import cn.jfast.expand.wx.api.msg.PicInfo;
import cn.jfast.expand.wx.api.msg.ReceiveMsg;
import cn.jfast.framework.log.LogFactory;
import cn.jfast.framework.log.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * 微信消息内容处理器
 */
public class XMLHandler extends DefaultHandler {

    private static final Logger log = LogFactory.getLogger(XMLHandler.class);

	/**
	 * 消息实体定义
	 */
	private ReceiveMsg msg = new ReceiveMsg();

    /**
     * 图片信息
     */
    private PicInfo picInfo;

    private List<PicInfo> picList;

    /**
     * 节点属性值
     */
    private String attrVal;

	/**
	 * 获取消息实体对象
     *
     * @return 带数据的消息实体
	 */
	public ReceiveMsg getMsgVO() {
		return this.msg;
	}

    @Override
    public void startElement(String uri,
                             String localName,
                             String qName,
                             Attributes attributes) throws SAXException {  	
        if(qName.equals("PicList"))
        	this.picList = new ArrayList<PicInfo>();
        else if(qName.equals("item"))
        	this.picInfo = new PicInfo();
    }

    @Override
    public void endElement(String uri,
                           String localName,
                           String qName) throws SAXException {

        if (log.isInfoEnable()) {
            if (!"xml".equals(qName)) {
                log.info("当前节点值[%s]: %s", qName, attrVal);
            }
        }
        if(qName.equals("MsgId")||qName.equals("MsgID"))
        	msg.setMsgId(Long.valueOf(attrVal));
        else if(qName.equals("CreateTime"))
        	msg.setCreateTime(Long.valueOf(attrVal));
        else if(qName.equals("MsgType"))
        	msg.setMsgType(attrVal);
        else if(qName.equals("Event"))
        	msg.setEvent(attrVal);
        else if(qName.equals("ToUserName"))
        	msg.setToUserName(attrVal);
        else if(qName.equals("FromUserName"))
        	msg.setFromUserName(attrVal);
        else if(qName.equals("Content"))
        	msg.setContent(attrVal);     
        else if(qName.equals("PicUrl"))
        	 msg.setPicUrl(attrVal);
        else if(qName.equals("MediaId"))
        	msg.setMediaId(attrVal);
        else if(qName.equals("Format"))
        	msg.setFormat(attrVal);
        else if(qName.equals("Recognition"))
        	msg.setRecognition(attrVal);      
        else if(qName.equals("ThumbMediaId"))
        	msg.setThumbMediaId(attrVal);
        else if(qName.equals("Location_X") || qName.equals("Latitude"))
        	msg.setLatitude(Double.valueOf(attrVal));
        else if(qName.equals("Location_Y") || qName.equals("Longitude"))
        	msg.setLongitude(Double.valueOf(attrVal));
        else if(qName.equals("Recognition"))
        	msg.setRecognition(attrVal);
        else if(qName.equals("Scale"))
        	msg.setScale(Integer.parseInt(attrVal));
        else if(qName.equals("Label"))
        	msg.setLabel(attrVal);
        else if(qName.equals("Title"))
        	msg.setTitle(attrVal);
        else if(qName.equals("Description"))
        	msg.setDescription(attrVal);
        else if(qName.equals("Url"))
        	msg.setUrl(attrVal);
        else if(qName.equals("EventKey"))
        	msg.setEventKey(attrVal);
        else if(qName.equals("Ticket") || qName.equals("ComponentVerifyTicket"))
        	msg.setTicket(attrVal);
        else if(qName.equals("Precision"))
        	msg.setPrecision(Double.valueOf(attrVal));
        else if(qName.equals("ScanType"))
        	msg.setScanType(attrVal);
        else if(qName.equals("ScanResult"))
        	msg.setScanResult(attrVal);
        else if(qName.equals("Count"))
        	msg.setCount(Integer.parseInt(attrVal));
        else if(qName.equals("PicMd5Sum"))
        	picInfo.setPicMd5Sum(attrVal);
        else if(qName.equals("item"))
        	picList.add(picInfo);
        else if(qName.equals("PicList"))
        	msg.setPicList(picList);
        else if(qName.equals("Poiname"))
        	msg.setPoiName(attrVal);
        else if(qName.equals("Status"))
        	msg.setStatus(attrVal);
        else if(qName.equals("TotalCount"))
        	msg.setTotalCnt(Integer.parseInt(attrVal));
        else if(qName.equals("FilterCount"))
        	msg.setFilterCnt(Integer.parseInt(attrVal));
        else if(qName.equals("SentCount"))
        	msg.setSentCnt(Integer.parseInt(attrVal));
        else if(qName.equals("ErrorCount"))
        	msg.setErrorCnt(Integer.parseInt(attrVal));
        else if(qName.equals("AppId"))
        	msg.setAppId(attrVal);
        else if(qName.equals("InfoType"))
        	msg.setInfoType(attrVal);
        else if(qName.equals("AuthorizerAppid"))
        	msg.setUnAuthAppid(attrVal);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        this.attrVal = new String(ch, start, length);
    }

    /**
     * 清除当前VO对象缓存数据
     */
    public void clear() {
        this.picInfo = null;
        this.msg = new ReceiveMsg();
    }
}
