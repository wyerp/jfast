package cn.jfast.framework.base.util;

public enum ErrorCode {

    MULTI_ROW(1, "查询语句返回多行数据"),
    MULTI_COLUMN(2,"查询语句返回多列数据"),
    ERROR_METHOD_NAME(4, "方法名错误"),
    ERROR_RETURN_TYPE(4, "返回类型错误"),
    ERROR_PARAM_TYPE(5, "参数类型错误"),
    REPEAT_PARAM(6, "参数类型错误"),
    NO_SUCH_TABLE(7, "不存在对应表"),
    NO_FIELD_NEED_TO_UPDATE(8 , "更新语句没有字段需要更新"),
    NO_FIELD_NEED_TO_INSERT(9 , "插入语句没有字段需要插入"),
    NO_FIELD_NEED_TO_DELETE(10 , "删除语句没有有效条件字段"),
    NOT_SUPPORT_GET_GENERATEDKEY(11 , "不支持返回插入数据的自增编号"),
    NO_KEY_FIELD(12 , "更新语句缺少依据字段");

    private int errorCode;
    private String errorMsg;

    private ErrorCode(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
