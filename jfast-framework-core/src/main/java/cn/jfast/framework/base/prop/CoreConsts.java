/*
 * Copyright 2015 泛泛o0之辈
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.jfast.framework.base.prop;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 系统属性文件
 * @author 泛泛o0之辈
 */
public abstract class CoreConsts {

	/** 打印info日志 */
	public static boolean info_enable = true;
	/** 打印debug日志 */
	public static boolean debug_enable = true;
	/** 打印warn日志 */
	public static boolean warn_enable = true;
	/** 打印error日志 */
	public static boolean error_enable = true;
	/** 支持JS跨域访问 */
	public static boolean cors_mode = true;
	/** 开发模式 */
	public static boolean develop_mode = true;
	/** 系统编码 */
	public static String encoding = "UTF-8";
	/** 微信消息处理器 */
	public static String msg_handler;
	/** 微信access_token存储器,需要使用集群环境时使用 */
	public static String access_token_holder;
	/** 微信账号信息资源类--用于多微信号环境 */
	public static String mp_act_resource;
	/** Java运行时环境版本 */
	public static String javaVersion = System.getProperty("java.version");
	/** Java运行时环境提供商 */
	public static String javaVendor = System.getProperty("java.vendor");
	/** Java运行时环境提供商URL */
	public static String javaVendorUrl = System.getProperty("java.vendor.url");
	/** Java安装目录 */
	public static String javaHome = System.getProperty("java.home");
	/** Java虚拟机规范版本 */
	public static String javaVmSpecificationVersion = System.getProperty("java.vm.specification.version");
	/** Java虚拟机规范提供商 */
	public static String javaVmSpecificationVendor = System.getProperty("java.vm.specification.vendor");
	/** Java虚拟机规范名称 */
	public static String javaVmSpecificationName = System.getProperty("java.vm.specification.name");
	/** Java虚拟机版本 */
	public static String javaVmVersion = System.getProperty("java.vm.version");
	/** Java虚拟机提供商 */
	public static String javaVmVendor = System.getProperty("java.vm.vendor");
	/** Java虚拟机名称 */
	public static String javaVmName = System.getProperty("java.vm.name");
	/** Java类格式版本号 */
	public static String javaClassVersion = System.getProperty("java.class.version");
	/** Java类路径 */
	public static String javaClassPath = System.getProperty("java.class.path");
	/** 加载库时搜索列表 */
	public static String javaLibraryPath = System.getProperty("java.library.path");
	/** 默认临时文件路径 */
	public static String javaIoTmpdir = System.getProperty("java.io.tmpdir");
	/** 操作系统名称 */
	public static String osName = System.getProperty("os.name");
	/** 操作系统架构 */
	public static String osArch = System.getProperty("os.arch");
	/** 操作系统版本 */
	public static String osVersion = System.getProperty("java.version");
	/** 用户账户名称 */
	public static String userName = System.getProperty("user.name");
	/** 用户主目录 */
	public static String userHome = System.getProperty("user.home");
	/** 用户当前工作目录 */
	public static String userDir = System.getProperty("user.dir");
	/** 服务器IP */
	public static String hostIP = getIP();
	/** 服务器主机名 */
	public static String hostName = getHostName();
	/** 本地Web地址 */
	public static String SERVLET_BASE_PATH = "";


	private static String getIP(){
		String ip = "";
		try {
		  ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ip;
	}

	private static String getHostName(){
		String name = "";
		try {
			name = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return name;
	}

}
